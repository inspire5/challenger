import React, {useState} from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, CheckBox } from 'react-native';
import  * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('db.challenger');

const regexU = /^[a-zA-Z0-9._]+$/;
const eightCorrectChar = /^[a-zA-Z0-9]{8,}$/;
const haveDigit = /\d/;
const haveLowercase = /[a-z]/;
const haveUppercase = /[A-Z]/;

export function LoginInputArea(props) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isSelected, setIsSelected] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    function inputNotEmpty() {
      if (username != "" && password != ""){
        return true;
      } else {return false;}
    }

    function inputValidate(){
      if (!username.match(regexU)){
        alert("Invalid Username/Password! \nPlease Try Again.");
        setErrorMessage("*Your username should include only letters (a-z,A-Z), numbers (0-9), underscores (_), and periods (.)");
        return false;
      } 
      else if (!password.match(eightCorrectChar)){
        alert("Invalid Username/Password! \nPlease Try Again.");
        setErrorMessage("*Your password should have at least 8 characters that are either letters (a-z,A-Z) or numbers (0-9)");
        return false;
      } 
      else if (!haveDigit.test(password)){
        alert("Invalid Username/Password! \nPlease Try Again.");
        setErrorMessage("*Your password should have at least 1 number (0-9)");
        return false;
      }
      else if (!haveLowercase.test(password)){
        alert("Invalid Username/Password! \nPlease Try Again.");
        setErrorMessage("*Your password should have at least 1 lowercase letter (a-z)");
        return false;
      }
      else if (!haveUppercase.test(password)){
        alert("Invalid Username/Password! \nPlease Try Again.");
        setErrorMessage("*Your password should have at least 1 uppercase letter (A-Z)");
        return false;
      }
      else {
        return true;
      }
    }

    function verifyWithDb () {
      db.transaction((tx) => {
          tx.executeSql("SELECT 1 FROM loginInfo WHERE username = ? AND password = ?", [username, password], 
            (_, {rows}) => {
              if (rows.length == 0) {
                setErrorMessage('*Invalid Login Details');
              } else {
                props.navigate.navigate('TaskScreen', {username: username})
              }
            });
        }
      );
    }

    function login(){
      if (inputValidate()) {
        verifyWithDb();
      }
    }

    return (
      <View style = {styles.loginInputArea}>
        <View style = {styles.usernameInputView}>
          <TextInput 
            style = {styles.textInput}
            placeholder = "Username"
            placeholderTextColor = "#FFFFFF"
            onChangeText = {(username) => setUsername(username)}
            value = {username}
            />
        </View>

        <View style = {styles.passwordInputView}>
          <TextInput 
            style = {styles.textInput}
            placeholder = "Password"
            placeholderTextColor = "#FFFFFF"
            secureTextEntry = {true}
            onChangeText = {(password) => setPassword(password)}
            value = {password}
            />
        </View>

        <Text style= {(errorMessage != '') ? styles.inputErrorMessage : styles.noErrorMessage}>{errorMessage}</Text>

        <View style = {styles.checkBoxContainer}>
          <CheckBox 
            value={isSelected}
            onValueChange={setIsSelected}
            />
          <Text style = {styles.label}>Remember Me</Text>
        </View>

        <TouchableOpacity 
          disabled = {!inputNotEmpty()} 
          style={inputNotEmpty() ? styles.loginBtnAct : styles.loginBtnDisAct} 
          onPress = {()=>login()}
          >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    );
  }

  const styles = StyleSheet.create({
    loginInputArea:{
      width: "65%",
    },
    usernameInputView: {
      backgroundColor: "#FFB09A",
      borderRadius: 17,
      height: 43,
      marginBottom: 20,
      borderWidth: 1, 
    },
    textInput: {
      padding: 10,
      opacity: 0.76 ,
    },
    passwordInputView: {
      backgroundColor: "#FFB09A",
      borderRadius: 17,
      height: 43,
      marginBottom: 5,
      borderWidth: 1, 
    },
    loginBtnAct: {
      backgroundColor: "#FFCF8E",
      borderRadius: 17,
      height: 43,
      marginBottom: 10,
      borderWidth: 1, 
      alignItems: "center",
    },
    loginBtnDisAct: {
      backgroundColor: "#FFCF8E",
      borderRadius: 17,
      height: 43,
      marginBottom: 10,
      borderWidth: 1, 
      alignItems: "center",
      opacity: 0.5,
    },
    loginText: {
      justifyContent: 'center',
      color: "#FFFFFF",
      padding: 10,
    },
    checkBoxContainer: {
      flexDirection: "row",
      paddingRight: "33%",
      marginBottom: 50,
    },
    label: {
      marginTop: 5,
    },
    inputErrorMessage:{
      fontSize: 12,
      color: "#FF0000",
      width: "65%",
    },
    noErrorMessage:{
      margin: -10,
    },
  });