import 'react-native-gesture-handler';
import { StatusBar } from 'expo-status-bar';
import React, {useEffect} from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import  * as SQLite from 'expo-sqlite';
import { LoginInputArea } from './LoginInputArea';

const db = SQLite.openDatabase('db.challenger');

export function LoginScreen({navigation}) {  

  useEffect(() => {
    db.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS loginInfo (username VARCHAR(50) PRIMARY KEY NOT NULL, password VARCHAR(50));"
      );
      tx.executeSql(
        "INSERT INTO loginInfo (username, password) VALUES ('cinnie','INspire2021');"
      );
    });
    // db.transaction((tx)=>{
    //   tx.executeSql(
    //     "SELECT * FROM loginInfo", [], (_, {rows}) => console.log(JSON.stringify(rows))
    //   );
    // });
  }, []);

  return (
    <View style={styles.container}>
      <Image 
          source = {require("../assets/logo.jpg")}
          style = {styles.logo} />

      <LoginInputArea navigate={navigation}/>

      <TouchableOpacity>
          <Text style={styles.forgotBtn}>Forgot Password?</Text>
      </TouchableOpacity>

      <TouchableOpacity>
          <Text style={styles.newAccountBtn}>Create an Account?</Text>
      </TouchableOpacity>

      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    flex: .25,
    width: "90%",
    resizeMode: 'cover',
    marginBottom: 50
  },
  forgotBtn: {
    color: "#6694D6",
    textDecorationLine: "underline",
    marginBottom: 5,
  },
  newAccountBtn: {
    color: "#6694D6",
    textDecorationLine: "underline",
  },
});