import React from 'react';
import {Text, StyleSheet} from 'react-native';

export function TaskDate(props){
    return(
        <Text style={styles.date}>{props.taskdate}</Text>      
    );
}

const styles = StyleSheet.create({
    date:{
        fontSize: 17,
        color: "#515151",
        textDecorationLine: "underline",
        marginBottom: 15,
        paddingLeft: 5,
    },
});