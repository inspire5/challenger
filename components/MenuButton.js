import {StyleSheet, View, Button} from 'react-native';
import React from 'react';

export function MenuButton(){
    return (
        <View style={styles.toDoMenuButton}>
            <Button color="#ffa64d" title="Menu" onPress={()=> alert("A Button!")}/>
        </View>
    );
}

const styles = StyleSheet.create({
    toDoMenuButton: {
      marginRight: 10,
    },
  });