import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {LoginScreen} from './components/LoginScreen';
import {TaskScreen} from './components/TaskScreen';
import {MenuButton} from './components/MenuButton';

let optionSetting = {
  headerLeft: null, 
  headerRight: () => <MenuButton/>,
  headerTintColor: "#FFFFFF", 
  headerStyle: {backgroundColor: "#faad60", height: 85}
}

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name = "LoginScreen" component = {LoginScreen} options={{headerShown: false}}/>
        <Stack.Screen name = "TaskScreen" component = {TaskScreen} options={{...optionSetting, title: "TO-DO LIST"}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
