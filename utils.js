export const executeDb = (sqlStatement, args) => {
    return new Promise((resolve)=>{
        db.transaction((tx) => {
            tx.executeSql(
                sqlStatement, args, (_, response) => {
                    resolve(response);
                }
            );
        });
    });
}
