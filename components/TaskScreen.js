import 'react-native-gesture-handler';
import React , {useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ScrollView, StatusBar} from 'react-native';
import {Task} from './Task';
import {TaskDate} from './TaskDate';
import  * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('db.challenger');

export function TaskScreen ({route: {params}}){
    
    const [taskItems, setTaskItems] = useState([]);

    useEffect(() => {
        db.transaction((tx) => {
            tx.executeSql(
                "CREATE TABLE IF NOT EXISTS tasks (id INTEGER PRIMARY KEY AUTOINCREMENT, done int, title text, description text);", [], undefined, console.log
            );
        }, undefined, () => refreshTasks());
    }, []);

    function refreshTasks(){
        db.transaction((tx) => {
            tx.executeSql(
                "SELECT id, title, description, done FROM tasks;", [], (_,{rows}) => setTaskItems(rows._array)
            );
        });
    }

    async function addTask(){
        db.transaction((tx) => {
            tx.executeSql(
                "INSERT INTO tasks (done) VALUES (0);" 
            );
        }, console.log, () => refreshTasks());
    }

    const username = params?.username;

    console.log(taskItems);
    
    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content"/>
            <ScrollView>
                <View style={styles.textWrap}>
                    <Text>Hi, {username}</Text>
                    <TaskDate taskdate={"30-05-2021"}/>
                    {
                        taskItems.map(({id, title, description, done}) => {
                            return (
                                <Task key={id} id={id} title={title} description={description} done={done} refresh={refreshTasks}/>
                            )
                        })
                    }
                </View>
            </ScrollView>
            <TouchableOpacity style={styles.buttonWrapper} onPress={() => addTask()}>
                <View style={styles.addButton}>
                    <Text style={styles.addSign}>+</Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#ffddb0",
    },  
    textWrap:{
        paddingVertical: 20,
        paddingHorizontal: 20,
    },
    buttonWrapper: {
        position: "absolute",
        right: 30,
        bottom: 30,
    },
    addButton:{
        backgroundColor: "#faad60",
        borderRadius: 30,
        width: 60,
        height: 60,
        alignItems: "center",
        justifyContent: "center",
        paddingBottom: 3,
    },
    addSign:{
        color: "#FFFFFF",
        fontSize: 30,
    },
});