import React, {useState} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import  * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('db.challenger');

export function Task(props){
    const [taskTitle, setTaskTitle] = useState(props.title || '');
    const [taskDescription, setTaskDescription] = useState(props.description || '');

    function updateTaskDb(){
        db.transaction((tx) => {
            tx.executeSql(
                "UPDATE tasks SET title = ?, description = ? WHERE id = ?", [taskTitle, taskDescription, props.id], console.log, console.log
            );
        }, console.log, () => props.refresh());
    }

    return(
        <View style={styles.taskBlock}>
            <TextInput 
                key = {`${props.id}-title`}
                style={styles.taskTitle} 
                placeholder={"Task Title..."} 
                onChangeText={(title) => setTaskTitle(title)}
                onSubmitEditing={() => updateTaskDb()}
                value = {taskTitle}
                />
            <Text style={styles.lineSeparate}>_____________</Text>
            <TextInput 
                key = {`${props.id}-description`}
                style={styles.taskDescription} 
                placeholder={"Description Here..."} 
                onChangeText={(description) => setTaskDescription(description)}
                onSubmitEditing={() => updateTaskDb()}
                value = {taskDescription}
                />
        </View>       
    );
}

const styles = StyleSheet.create({
    taskBlock:{
        backgroundColor: "#fffbe0",
        borderColor: "#B0B0B9",
        borderWidth: 1,
        padding: 10,
        borderRadius: 13,
        marginBottom: 12,
    },
    taskTitle:{
        fontWeight: "bold",
        fontSize: 18,
        marginBottom: -10,
        width: "80%",
    },
    lineSeparate:{
        color: "#fccdb3",
        marginBottom: 3,
    },
    taskDescription:{
        color: "#7D7D7D",
        fontSize: 16,
        width: "70%",
        flexWrap: "wrap",
    },
});